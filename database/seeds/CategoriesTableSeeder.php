<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['cat one', 'cat two', 'cat three','php5', 'php6', 'php7','php3', 'php11', 'php3'];

        foreach ($categories as $category) {

            \App\Category::create([
                'ar' => ['name' => $category],
                'en' => ['name' => $category],
            ]);

        }
    }//end of run
}
